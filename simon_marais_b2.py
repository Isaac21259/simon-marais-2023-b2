import numpy as np

def k(r, c):
    if 2*r - 1 == c:
        return 3/5
    elif 2*r == c:
        return 2/5
    else:
        return 0

def transition(roun):
    return np.array([[k(r, c) for c in range(1, 2**(9 - roun) + 1)] for r in range(1, 2**(8 - roun) + 1)])

initState = np.array([i for i in range(1, 256 + 1)], dtype = float)

def runRound(s, roun):
    return transition(roun) @ s

def runRounds(n):
    if n == 0:
        return initState
    else:
        return runRound(runRounds(n - 1), n)

answer = runRounds(8)